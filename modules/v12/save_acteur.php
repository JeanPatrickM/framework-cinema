<?php
require_once "acteur_service.php";

/**
 * Class Save_acteur | file Save_acteur.php
 *
 * In this class, we show the interface "Save_acteur.html".
 * With this interface, we'll be able to save a new actor
 *
 * @package Cinema Project
 * @subpackage configuration
 * @author @Afpa Lab Team
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */
class Save_acteur	{
	
	/**
	 * public $resultat is used to store all datas needed for HTML Templates
	 * @var array
	 */
	public $resultat;

	/**
	 * init variables resultat
	 *
	 * execute main function
	 */
	public function __construct()	{
		// init variables resultat
		$this->resultat= [];

		// execute main function
		$this->main();
	}

	/**
	 * Add an actor in the database
	 */
	function main()	{
		// Create a new acteur
		$obj_save_acteur = new Acteur_service();
		$obj_save_acteur->save_acteur();

		$this->resultat= $obj_save_acteur->resultat;
		$this->VARS_HTML= $obj_save_acteur->VARS_HTML;
	}
}

?>

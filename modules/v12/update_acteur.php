<?php
require_once "acteur_service.php";

/**
 * Class Update_acteur | file Update_acteur.php
 *
 * In this class, we show the interface "Update_acteur.html".
 * With this interface, we'll be able to update an actor with its id
 *
 * @package Cinema Project
 * @subpackage configuration
 * @author @Afpa Lab Team
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */
class Update_acteur	{
	
	/**
	 * public $resultat is used to store all datas needed for HTML Templates
	 * @var array
	 */
	public $resultat;

	/**
	 * init variables resultat
	 *
	 * execute main function
	 */
	public function __construct()	{
		// init variables resultat
		$this->resultat= [];

		// execute main function
		$this->main();
	}

	/**
	 * Update an actor with its id
	 */
	function main()	{
		$objet_update_acteur = new Acteur_service();
		$objet_update_acteur->update_acteur();

		$this->resultat= $objet_update_acteur->resultat;
		$this->VARS_HTML= $objet_update_acteur->VARS_HTML;
	}
}
?>

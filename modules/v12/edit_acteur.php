<?php
require_once "acteur_service.php";

/**
 * Class Edit_acteur | file edit_acteur.php
 *
 * In this class, we show the interface "edit_acteur.html".
 * With this interface, we'll be able to edit an actor with its id
 *
 * @package Cinema Project
 * @subpackage configuration
 * @author @Afpa Lab Team
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */
class Edit_acteur	{
	
	/**
	 * public $resultat is used to store all datas needed for HTML Templates
	 * @var array
	 */
	public $resultat;

	/**
	 * init variables resultat
	 *
	 * execute main function
	 */
	public function __construct()	{
		// init variables resultat
		$this->resultat= [];

		// execute main function
		$this->main();
	}

	/**
	 * Get datas from database and edit an actor with its id
	 */
	function main()	{
		$objet_edit_acteur = new acteur_service();
		$objet_edit_acteur -> edit_acteur();

		$this->resultat = $objet_edit_acteur->resultat;
	}
}

?>
